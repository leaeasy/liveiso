#!/bin/bash
# Upload dailylive ISO to cdimage

DIST=$1

case $DIST in 
	sid)
	DAILY_LIVE_DIR="/srv/iso/sid/"
	;;
	*)
	echo "Not support $DIST"
	return
	;;
esac

echo "Upload dailylive iso to cdimage"
rsync -rtlvp --progress ${DAILY_LIVE_DIR}/ cdimage:/data/iso/daily-live-$DIST

echo "Update pxe server"
ssh cdimage "~/pxe/bin/update-deepin-pxe.sh amd64 i386"
echo ""
