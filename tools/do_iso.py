#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Remove to old iso file to save space.

import os
from datetime import datetime

def processfile(filename, expire=7):
    if not os.path.isfile(filename):
        return
    _create_time = datetime.fromtimestamp(os.path.getctime(filename))
    _intervaldays = (datetime.now() - _create_time).days
    # the today file not handle
    if not _intervaldays:
        print("Skip Now removing %s" % filename)
        return

    if _intervaldays > expire:
        print("Now removing %s" % filename)
        os.remove(filename)
        basename = os.path.basename(filename)
        if not os.listdir(basename):
            os.rmdir(basename)
    else:
        print("Skip removing %s" % filename)


def listfile(path, file_suffix="iso"):
    if os.path.isdir(path) and os.path.exists(path):
        _file_suffix = "." + file_suffix
        for i, _, j in os.walk(path):
            for f in j:
                if f.endswith(_file_suffix):
                    yield os.path.join(i, f)

if __name__ == "__main__":
    for file in listfile("/srv/iso", file_suffix="iso"):
        processfile(file)
