#!/bin/bash
DIR=$1
if [ -d $DIR ];then
	find $1  -maxdepth 1 -mindepth 1  -type d -ctime 3 -exec rm -rf {} \;
fi
