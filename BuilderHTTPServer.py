#!/usr/bin/env python3
import flask
from cork import Cork
from BuilderSqlbackend import SQLiteBackend
from BuilderConfig import BuilderConfig
from BuilderGit import BuilderGit
import os
import datetime
import math

cfg = BuilderConfig()
backend = SQLiteBackend(cfg.get('build','database'))
aaa = Cork(backend=backend, email_sender=cfg.get('mail', 'mail_from'), smtp_url=cfg.get('mail','smtp_host'))

app = flask.Flask(__name__)
app.debug = True
app.options = {}

from flask import jsonify

def post_get(name, default=''):
    v = flask.request.form.get(name, default).strip()
    return str(v)

from cork import Redirect
@app.errorhandler(Redirect)
def redirect_exception_handler(e):
    return flask.redirect(str(e))

@app.route('/login', methods=['POST'])
def login():
    username = post_get('username')
    password = post_get('password')
    aaa.login(username, password, success_redirect='/', fail_redirect='/login')

@app.route('/job/<int:job_id>', methods=['GET'])
def job(job_id):
    try:
        current_user = aaa.current_user
    except:
        current_user = None
    for job in backend.jobs.iteritems():
        if int(job[0]) == job_id:
            return flask.render_template('job.html', job=job, current_user=current_user)
    return flask.render_template('404.html'),404

@app.route('/jobs/<int:jobs_id>', methods=['GET'])
@app.route('/jobs', methods=['GET'])
def jobs(jobs_id=None):
    if jobs_id is None:
        jobs_id = 1
    try:
        current_user = aaa.current_user
    except:
        current_user = None

    jobs = []
    _length = 0
    for job in backend.jobs.iteritems():
        _length +=1
        if _length > (jobs_id - 1) * 10 and _length < jobs_id * 10 + 1:
            jobs.append(job)
    length=math.ceil(_length/10)
    
    return flask.render_template('jobs.html',current_user=current_user, jobs=jobs, length=length, jobs_id=jobs_id)

@app.route('/job/delete/<int:jobs_id>')
def delete_job(jobs_id):
    try:
        backend.jobs.delete(jobs_id)
        return jsonify(ok=True, msg="delete job %d successful" % jobs_id)
    except Exception as e:
        return jsonify(ok=False, msg="delete job %d failed\n %s" % (jobs_id, e))

@app.route('/')
def index():
    #aaa.require(fail_redirect='/login')
    branchs = None
    jobs = []
    try:
        current_user = aaa.current_user
    except:
        current_user = None
    if current_user:
        try:
            branchs = BuilderGit(current_user.username, current_user.git).branchs 
        except:
            pass
        num = 1
        for job in backend.jobs.iteritems():
            if num > 10:
                break
            if job[1]['create_user'] == current_user.username:
                num += 1
                jobs.append(job)
    return flask.render_template('index.html', current_user=current_user, branchs=branchs, jobs=jobs)

@app.route('/my')
def show_current_user():
    aaa.require(fail_redirect='/login')
    return flask.render_template('my.html', current_user=aaa.current_user)

@app.route('/modify_user', methods=['POST'])
def modify_user():
    aaa.require(fail_redirect='/login')
    try:
        if post_get('password'):
            aaa.current_user.update(pwd=post_get('password'), email_addr=post_get('email'), git=post_get('git'))
        else:
            aaa.current_user.update(email_addr=post_get('email'), git=post_get('git'))

        return jsonify(ok=True, msg='Modify user successfully')
    except Exception as e:
        return jsonify(ok=False, msg=str(e))


@app.route('/log/<string:logfile>')
def log(logfile):
    _logfile = os.path.join(cfg.get('log','logs_dir'), logfile)
    if os.path.exists(_logfile):
        return '</br>'.join(open(_logfile).readlines())
    else:
        return _logfile

@app.route('/login')
def login_form():
    return flask.render_template('login_form.html', current_user=None)

@app.route('/help')
def help():
    try:
        current_user = aaa.current_user
    except:
        current_user = None
    return flask.render_template('help.html', current_user=current_user)

@app.route("/create_job", methods=['POST'])
def create_job():
    aaa.require(fail_redirect='/sorry_page')
    _timestamp = datetime.datetime.utcnow()
    current_user = aaa.current_user
    _update_pxe = 1 if post_get('update_pxe') == 'on'  else 0
    _mail_successful = 1 if post_get('mail_successful') == 'on' else 0
    _mail_failed = 1 if post_get('mail_failed') == 'on' else 0
    _branch = post_get('branch','')
    _iso_prefix = "deepin-sid"
    if _branch:
        _iso_prefix = "deepin-sid" + "-" + _branch

    if post_get('build_all') == 'on':
        for _arch in ['amd64', 'i386']:
            backend.jobs.append({
                'name': _iso_prefix,
                'create_date': str(_timestamp),
                'create_user': current_user.username,
                'arch': _arch,
                'branch': _branch,
                'conf': 'dummy',
                'start_time': '',
                'end_time': '',
                'status': 0,
                'iso': '',
                'log': current_user.username + '_' + str(_timestamp.timestamp())+ '.log',
                'desc': "Build %s" % _arch,
                'update_pxe': _update_pxe,
                'mail_successful': _mail_successful ,
                'mail_failed': _mail_failed
                })

    else:
        backend.jobs.append({
            'name': _iso_prefix,
            'create_date': str(_timestamp),
            'create_user': current_user.username,
            'arch': '',
            'branch': _branch,
            'conf': 'dummy',
            'start_time': '',
            'end_time': '',
            'status': 0,
            'iso': '',
            'log': current_user.username + '_' + str(_timestamp.timestamp())+ '.log',
            'desc': "Build native",
            'update_pxe': _update_pxe,
            'mail_successful': _mail_successful ,
            'mail_failed': _mail_failed
            })

    return "add job successfully"

@app.route('/logout')
def logout():
    aaa.logout(success_redirect='/login')

@app.route('/admin')
def admin():
    aaa.require(role='admin', fail_redirect='/sorry_page')
    return flask.render_template('admin_page.html',
            current_user=aaa.current_user,
            users=aaa.list_users(),
            roles=aaa.list_roles()
            )

@app.route('/create_user', methods=['POST'])
def create_user():
    try:
        aaa.create_user(post_get('username'), post_get('role'), post_get('password'), email_addr=post_get('email'), description=post_get('description'))
        return jsonify(ok=True, msg='')
    except Exception as e:
        return jsonify(ok=False, msg=str(e))

@app.route('/sorry_page')
def sorry_page():
    return '<p>Sorry, you are not authorized to perform this action</p>'


def main():
    #app.secret_key = os.urandom(24)
    app.secret_key = "!@#$%^QWERT"
    app.run(host='0.0.0.0', debug=True, use_reloader=True)

if __name__ == "__main__":
    main()
