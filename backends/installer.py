#! /usr/bin/env python3
#

import os
import subprocess
import sys

# TODO:auto detect program abspath.
Path=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(Path)

import misc
from misc import environment, execute
from exceptions import DebootstrapException, InitException, ExecutionException
import glob

class MiniInstaller():
    def __init__(self, architecture=None):
        if not os.path.isfile('config/common'):
            raise InitException("config/common not exists")

        self.path = Path
        self.environment = environment('config/common')
        self.architecture = architecture if architecture else self.environment.get('LB_ARCH')
        if self.architecture is None:
            _arch = subprocess.check_output("dpkg --print-architecture", shell=True).strip()
            self.architecture = bytes.decode(_arch)
        self.distribution = self.environment.get('LB_DISTRIBUTION')
        assert self.distribution is not None
        self.build_path = 'installer'

    def chroot_config(self):
        self.env = os.environ.copy()
        for item in self.environment:
            self.env[item] = self.environment[item]
        self.env['LB_ARCHITECTURE'] = self.architecture
        self.env['PROGRAM'] = self.path
        self.env['LB_INSTALLER'] = 'installer'

        os.makedirs('.installer', exist_ok=True)
        open('.installer/config', 'w').close()

    def debootstrap(self,debootrap_mirror=None):
        if not os.path.isfile('/usr/sbin/debootstrap'):
            raise DebootstrapException('/usr/sbin/debootstrap not exist.')

        debootrap_mirror = debootrap_mirror or self.env.get('LB_DEBOOTSTRAP_MIRROR')
        if debootrap_mirror is None:
            raise DebootstrapException("debootrap_mirror should not be none.")

        debootstrap_options_early = "--arch=" + self.architecture
        debootstrap_options_late = self.distribution + ' ' + self.build_path  + ' ' + debootrap_mirror
        debootstrap_options_early = debootstrap_options_early + ' --components=' + self.env.get('LB_DEBOOTSTRAP_AREA').replace(' ',',')

        debootstrap_args = self.environment.get('LB_DEBOOTSTRAP_OPTIONS')
        if debootstrap_args:
            debootstrap_options = debootstrap_options_early + ' ' + debootstrap_args + ' ' + debootstrap_options_late
        else:
            debootstrap_options = debootstrap_options_early + ' ' + debootstrap_options_late

        if os.path.isfile('.installer/bootstrap'):
            print('I: bootstrap already done - noting to do')
            return

        use_tmpfs = self.environment.get('USE_TMPFS')
        if not os.path.exists(self.build_path):
            os.makedirs(self.build_path)
        if use_tmpfs == '1':
            if self.build_path.startswith('/'):
                _build_path = self.build_path
            else:
                _build_path = os.path.join(os.getcwd(), self.build_path)
            if not misc.path_is_mounted(_build_path):
                command = 'mount -t tmpfs installer %s' % self.build_path
                print('I: mount tmpfs to build path: %s ' % self.build_path)
                execute(command)
        execute('/usr/sbin/debootstrap ' + debootstrap_options)
        os.makedirs('.build', exist_ok=True)
        open('.installer/bootstrap', 'w').close()

    def setup_chroot(self):
        scripts = [ 'chroot_misc', 'chroot_apt', 'chroot_dpkg-divert', 'chroot_miscfs' ]
        for script in scripts:
            print(os.path.join(self.path, 'scripts/build', script))
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute("%s install" % os.path.join(self.path, 'scripts/build', script), env=self.env)

    def install_packages(self):
        scripts = [ 'installer_packages-list', 'installer_linux-image', 'installer_install-packages' ]
        for script in scripts:
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute("%s" % os.path.join(self.path, 'scripts/build', script), env=self.env)

    def run_chroot_hooks(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'installer_hooks')):
            execute(os.path.join(self.path, 'scripts/build', 'installer_hooks'), env=self.env)

    def run_includes_chroot(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'installer_includes')):
            execute(os.path.join(self.path, 'scripts/build', 'installer_includes'), env=self.env)

    def run_chroot_cleanup(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'installer_cleanup')):
            execute(os.path.join(self.path, 'scripts/build', 'installer_cleanup'), env=self.env)

    def generate_rootfs(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'installer_rootfs')):
            execute(os.path.join(self.path, 'scripts/build', 'installer_rootfs'), env=self.env)

    def demote_chroot(self):
        scripts = [ 'chroot_misc', 'chroot_apt', 'chroot_dpkg-divert', 'chroot_miscfs' ]
        for script in scripts:
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                subprocess.call("%s remove" % os.path.join(self.path, 'scripts/build', script), env=self.env, shell=True)

    def run(self):
        self.chroot_config()
        #self.debootstrap()
        #self.setup_chroot()
        #self.install_packages()
        #self.run_includes_chroot()
        #self.run_chroot_hooks()
        #self.demote_chroot()
        self.generate_rootfs()
    
if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-p", "--work-path", dest="workpath", help="Set work directory")
    parser.add_option("-a", "--arch", dest="arch", help="Set arch to build")
    (options, args) = parser.parse_args()
    if options.workpath is None:
        workpath = os.getcwd()
    else:
        workpath = options.workpath

    if not os.path.exists(os.path.join(workpath,'config')):
        print("WorkPath: %s is not exists" % os.path.join(workpath, 'config'))
        sys.exit(2)

    db = MiniInstaller(architecture=options.arch)
    _current_path = os.getcwd()
    _build_successful = False
    try:
        os.chdir(workpath)
        db.run()
        _build_successful = True
    except Exception as e:
        print(e)
        db.demote_chroot()
    finally:
        os.chdir(_current_path)

    if _build_successful is True:
        sys.exit(0)
    else:
        sys.exit(1)
