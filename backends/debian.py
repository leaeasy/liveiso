#! /usr/bin/env python3
#

import os
import subprocess
import sys

# TODO:auto detect program abspath.
Path=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(Path)

import misc
from misc import environment, execute
from misc import DebootstrapException, InitException, ExecutionException
import glob

class _Module():
    def __init__(self, module):
        self.path = Path
        self.module = module
        self.info = self._info()
        self.fakeroot = "%s-fakeroot" % module
        self.env = environment('config/common')
        self.env['PROGRAM'] = Path
        self.env['MODULE'] = module
        self.env['BASEON'] = None
        self.env['SQUASHFS'] = self.info.get("SQUASHFS")
        self.sqfs = {}
        self.order = [ ]

    def _info(self):
        result = {'ORDER':None, 'REQUIREDBY':None, 'SQUASHFS': None}
        _module = os.path.join('config', 'overlay', '%s.module' % self.module)
        if os.path.exists(_module):
            with open(_module) as fp:
                for line in fp.readlines():
                    if '=' in line:
                        key, value = line.strip().split('=',1)
                        if key in [ 'SQUASHFS', 'REQUIREDBY' ]:
                            result[key] = value.replace('"',"").replace("'","")
                        if key == "ORDER":
                            result[key] = value.replace('"',"").replace("'","").split()
        else:
            raise InitException("Overlay %s.module not found!" % self.module)

        return result

    def _build(self, env):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'overlay_install-packages')):
            execute(os.path.join(self.path, 'scripts/build', 'overlay_install-packages'), env=env)

    def build(self):
        _env = self.env.copy()
        _order = self.info.get("ORDER")
        if _order is None:
            _env['BASEON'] = "chroot"
            _env['Module'] = self.module
            self._build(env=_env)
            self.sqfs[self.module]= self.info["SQUASHFS"]
            self.order.append(self.module)
        else:
            for i, module in enumerate(_order):
                _m = Module(module)
                if i == 0:
                    _env['BASEON'] = "chroot"
                else:
                    _env['BASEON'] = Module(_order[i-1]).fakeroot
                _env['MODULE'] = module
                _m._build(env=_env)
                self.sqfs[_m.module] = _m.info["SQUASHFS"]
                self.order.append(module)

            
            _env['BASEON'] = Module(_order[-1]).fakeroot
            _env['MODULE'] = self.module
            self._build(env=_env)
            self.sqfs[self.module] = self.info["SQUASHFS"]
            self.order.append(self.module)

    def umount(self):
        self.order.reverse()
        for order in self.order:
            env = os.environ.copy()
            env['PROGRAM'] =self.env['PROGRAM']
            env['MODULE'] = order
            if os.path.exists(os.path.join(self.path, 'scripts/build', 'overlay_umount-miscfs')):
                execute(os.path.join(self.path, 'scripts/build', 'overlay_umount-miscfs'), env=env)

    def generate(self):
        env = os.environ.copy()
        env['PROGRAM'] = Path
        env['REQUIREDBY'] = self.info['REQUIREDBY']
        self.order.reverse()
        for fs in self.order:
            env['MODULE'] = Module(fs).module
            env['SQUASHFS'] = self.sqfs[fs]

            if os.path.exists(os.path.join(self.path, 'scripts/build', 'overlay_generate-sqfs')):
                execute(os.path.join(self.path, 'scripts/build', 'overlay_generate-sqfs'), env=env)

def Module(module, instaces={}):
    if module not in instaces:
        _m = _Module(module)
        instaces[module] = _m
    return instaces[module]

class DebianBuilder():
    def __init__(self, architecture=None, name=None):
        if not os.path.isfile('config/common'):
            raise InitException("config/common not exists")

        self.path = Path
        self.environment = environment('config/common')
        self.architecture = architecture if architecture else self.environment.get('LB_ARCH')
        if self.architecture is None:
            _arch = subprocess.check_output("dpkg --print-architecture", shell=True).strip()
            self.architecture = bytes.decode(_arch)
        self.distribution = self.environment.get('LB_DISTRIBUTION')
        assert self.distribution is not None
        self.build_path = 'chroot'
        self.name = name

    def chroot_config(self):
        self.env = os.environ.copy()
        for item in self.environment:
            self.env[item] = self.environment[item]
        self.env['LB_ARCHITECTURE'] = self.architecture
        self.env['PROGRAM'] = self.path

        os.makedirs('.build', exist_ok=True)
        open('.build/config', 'w').close()

    def debootstrap(self,debootrap_mirror=None):
        if not os.path.isfile('/usr/sbin/debootstrap'):
            raise DebootstrapException('/usr/sbin/debootstrap not exist.')

        debootrap_mirror = debootrap_mirror or self.env.get('LB_DEBOOTSTRAP_MIRROR')
        if debootrap_mirror is None:
            raise DebootstrapException("debootrap_mirror should not be none.")

        debootstrap_options_early = "--arch=" + self.architecture
        debootstrap_options_late = self.distribution + ' ' + self.build_path  + ' ' + debootrap_mirror
        debootstrap_options_early = debootstrap_options_early + ' --components=' + self.env.get('LB_DEBOOTSTRAP_AREA').replace(' ',',')

        debootstrap_args = self.environment.get('LB_DEBOOTSTRAP_OPTIONS')
        if debootstrap_args:
            debootstrap_options = debootstrap_options_early + ' ' + debootstrap_args + ' ' + debootstrap_options_late
        else:
            debootstrap_options = debootstrap_options_early + ' ' + debootstrap_options_late

        if os.path.isfile('.build/bootstrap'):
            print('I: bootstrap already done - noting to do')
            return

        use_tmpfs = self.environment.get('USE_TMPFS')
        if not os.path.exists(self.build_path):
            os.makedirs(self.build_path)
        if use_tmpfs == '1':
            tmpfs_size = self.environment.get('TMPFS_SIZE')
            if self.build_path.startswith('/'):
                _build_path = self.build_path
            else:
                _build_path = os.path.join(os.getcwd(), self.build_path)
            if (not misc.path_is_mounted(_build_path)) and tmpfs_size:
                command = 'mount -t tmpfs -o size=%s tmpfs %s' % (tmpfs_size, self.build_path)
                print('I: mount tmpfs to build path: %s ' % self.build_path)
                execute(command)
        execute('/usr/sbin/debootstrap ' + debootstrap_options)
        os.makedirs('.build', exist_ok=True)
        open('.build/bootstrap', 'w').close()

    def setup_chroot(self):
        scripts = [ 'chroot_misc', 'chroot_apt', 'chroot_dpkg-divert', 'chroot_miscfs' ]
        for script in scripts:
            print(os.path.join(self.path, 'scripts/build', script))
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute("%s install" % os.path.join(self.path, 'scripts/build', script), env=self.env)

    def install_packages(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'chroot_local-packages-list')):
            execute("%s install" % os.path.join(self.path, 'scripts/build', 'chroot_local-packages-list'), env=self.env)

        scripts = [ 'chroot_packages-list', 'chroot_linux-image', 'chroot_install-packages' ]
        for script in scripts:
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute("%s" % os.path.join(self.path, 'scripts/build', script), env=self.env)

        if os.path.exists(os.path.join(self.path, 'scripts/build', 'chroot_local-packages-list')):
            execute("%s remove" % os.path.join(self.path, 'scripts/build', 'chroot_local-packages-list'), env=self.env)

    def run_chroot_hooks(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'chroot_hooks')):
            execute(os.path.join(self.path, 'scripts/build', 'chroot_hooks'), env=self.env)

    def run_includes_chroot(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'chroot_includes')):
            execute(os.path.join(self.path, 'scripts/build', 'chroot_includes'), env=self.env)

    def overlay_filesystem(self):
        if self.env.get("LB_OVERLAY") and self.env.get("LB_OVERLAY") != "SKIP":
            modules = self.env.get("LB_OVERLAY_MODULES")
            if modules:
                _modules = modules.split()
            for module in _modules:
                Module(module).build()
            for module in _modules:
                Module(module).umount()
            for module in _modules:
                Module(module).generate()
        else:
            print("Skip generate overlay")

    def build_binary_repo(self):
        if os.path.exists(os.path.join(self.path, 'scripts/build', 'binary_packages-list')):
            execute(os.path.join(self.path, 'scripts/build', 'binary_packages-list'), env=self.env)

    def generate_rootfs(self):
        scripts = ['binary_includes', 'binary_linux-image', 'binary_chroot', 'binary_manifest', 'binary_archives', 'binary_rootfs', 'binary_syslinux', 'binary_efi_grub2', 'binary_disk', 'binary_iso' ]
        for script in scripts:
            if script == "binary_efi_grub2" and self.env.get("LB_SECURITY_BOOT") == '0':
                script = "binary_efi"

            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                execute(os.path.join(self.path, 'scripts/build', script), env=self.env)

    def demote_chroot(self):
        scripts = [ 'chroot_misc', 'chroot_apt', 'chroot_dpkg-divert', 'chroot_miscfs' ]
        for script in scripts:
            if os.path.exists(os.path.join(self.path, 'scripts/build', script)):
                subprocess.call("%s remove" % os.path.join(self.path, 'scripts/build', script), env=self.env, shell=True)

    def run(self):
        self.chroot_config()
        self.debootstrap()
        self.setup_chroot()
        self.install_packages()
        self.run_includes_chroot()
        self.run_chroot_hooks()
        self.overlay_filesystem()
        self.build_binary_repo()
        self.demote_chroot()
        self.generate_rootfs()
    
if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-p", "--work-path", dest="workpath", help="Set work directory")
    parser.add_option("-a", "--arch", dest="arch", help="Set arch to build")
    parser.add_option("-n", "--name", dest="name", help="Set iso name prefix")
    (options, args) = parser.parse_args()

    if options.workpath is None:
        workpath = os.getcwd()
    else:
        workpath = options.workpath

    support_actions = ['build', 'clean']

    action = None
    for arg in args:
        if arg in support_actions:
            action = arg
            break
    if action is None:
        print("No action speicied")
        sys.exit(2)

    if action == "clean":
        dirs = ['binary', 'chroot', '.build']
        for dir in dirs:
            if dir == "chroot" and misc.path_is_mounted(os.path.join(workpath, dir)):
                os.system("umount -l %s" % os.path.join(workpath, dir))
            else:
                os.system("rm -rf %s" % os.path.join(workpath, dir))

        if os.path.exists(os.path.join(workpath, 'overlay')):
            if misc.path_is_mounted(os.path.join(workpath, 'overlay')):
                os.system("umount -l %s" % os.path.join(workpath, 'overlay'))

            os.system("rm -rf %s" % os.path.join(workpath, 'overlay'))
        sys.exit(0)

    if not os.path.exists(os.path.join(workpath,'config')):
        print("WorkPath: %s is not exists" % os.path.join(workpath, 'config'))
        sys.exit(2)

    db = DebianBuilder(architecture=options.arch, name=options.name)
    _current_path = os.getcwd()
    _build_successful = False
    try:
        os.chdir(workpath)
        db.run()
        _build_successful = True
    except Exception as e:
        print(e)
        db.demote_chroot()
    finally:
        os.chdir(_current_path)

    if _build_successful is True:
        sys.exit(0)
    else:
        sys.exit(1)
