#!/bin/bash

iso=$1

user=$2
title=$3

case $(basename $iso) in
    *amd64*)
	_arch=amd64
	;;
    *i?86)
	_arch=i386
	;;
    *)
	;;
esac

outdir=/srv/pxeroot/${user}/${title}-${_arch}/

[ -d $outdir ] && rm -rf $outdir

7z x $iso -o$outdir

chmod 755 $outdir -R
