#! /usr/bin/env python3
import os
import subprocess
import configparser

def environment(path):
    result = {}
    with open(path) as fp:
        for line in fp.readlines():
            line = line.strip()
            if line is None or line.startswith('#') or line == "":
                pass
            else:
                key, value = line.split('=',1)
                result[key] = value
    return result

class conf(configparser.ConfigParser):
    def __init__(self, defaults=""):
        super().__init__(defaults=defaults)

    def optionxform(self, optionstr):
        return optionstr

def path_is_mounted(path, fstype='tmpfs'):
    with open('/proc/self/mounts', 'r') as mtab:
        for line in mtab.readlines():
            _line = line.split()
            if path == _line[1] and fstype == _line[2]:
                return True
    return False

def umount_path_child(path):
    with open('/proc/self/mounts', 'r') as mtab:
        mounted_path=[]
        for line in mtab.readlines():
            _line = line.split()
            if _line[1].startswith(path) and _line[1] != path:
                mounted_path.append(_line[1])
        mounted_path.sort(reverse=True)
    for p in mounted_path:
        execute("umount -f %s" % p)

def execute(command, env=None):
    if env is not None:
        status = subprocess.call(command, env=env, shell=True)
    else:
        status = subprocess.call(command, shell=True)
    if status != 0:
        raise ExecutionException(command, status)

def round_off(path=None):
    if path is None:
        path=os.getcwd()
    try:
        umount_path_child(path)
        for folder in ['binary', 'chroot', '.build']:
            execute("rm -rf %s" % folder)
    except ExecutionException as e:
        print(e)
        pass

class InitException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return("E: Init exception - %s" % self.info)

class DebootstrapException(Exception):
    def __init__(self, info):
        self.info = info

    def __str__(self):
        return("E: Debootstrap exception - %s" % self.info)

class ExecutionException(Exception):
    def __init__(self, command, status, stderr=None):
        self.stderr = stderr
        self.status = status
        self.command = command

    def __str__(self):
        if self.stderr is not None:
            return ("'%s' returned exit status %s: %s" % (self.command, self.status, self.stderr))
        else:
            return ("'%s' returned exit status %s" % (self.command, self.status))
