#! /usr/bin/env python

import sqlite3
from BuilderLog import BuilderLog
import threading

class SqlRowProxy(dict):
    def __init__(self, table, key, row):
        li = ((k, v) for (k, ktype), v in zip(table._columns[1:], row[1:]))
        dict.__init__(self, li)
        self._table = table
        self._key = key

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        self._table[self._key] = self

class Table(object):
    def __init__(self, backend, table_name):
        self._backend = backend
        #self._engine = backend.connection
        self._table_name = table_name
        self._column_names = [ n for n, t in self._columns]
        self._key_col_num = 0
        self._key_col_name = self._column_names[self._key_col_num]
        self._key_col = self._column_names[self._key_col_num]

    def _row_to_value(self, key, row):
        assert isinstance(row, tuple)
        row_key = row[self._key_col_num]
        row_value = SqlRowProxy(self, key, row)
        return row_key, row_value

    def __len__(self):
        query = "SELECT count() FROM %s" % self._table_name
        ret = self._backend.fetch_one(query)
        return ret

    def __contains__(self, key):
        query = "SELECT * FROM %s WHERE %s='%s'" % (self._table_name, self._key_col, key)
        row = self._backend.fetch_one(query)
        return row is not None

    def __setitem__(self, key, value):
        assert isinstance(value, dict)
        v, cn = set(value), set(self._column_names[1:])
        assert not v - cn, repr(v - cn)
        assert not cn - v, repr(cn - v)
        assert set(value) == set(self._column_names[1:]), "%s %s" % (repr(set(value)), repr(set(self._column_names[1:])))
        col_values = [key] + [value[k] for k in self._column_names[1:]]
        col_names = ', '.join(self._column_names)
        question_marks = ', '.join('?' for x in col_values)
        query = "INSERT OR REPLACE INTO %s (%s) VALUES (%s)" % (self._table_name, col_names, question_marks)
        ret = self._backend.run_query_using_conversion(query, col_values)
        #self._engine.commit()

    def __getitem__(self, key):
        query = "SELECT * FROM %s WHERE %s='%s'" % (self._table_name, self._key_col, key)
        row = self._backend.fetch_one(query)
        if row is None:
            raise KeyError(key)
        return self._row_to_value(key, row)[1]

    def __iter__(self):
        query = "SELECT %s FROM %s" % (self._key_col, self._table_name)
        result = self._backend.run_query(query)
        for row in result:
            yield row[0]

    def iteritems(self):
        query = "SELECT * FROM %s" % self._table_name
        result = self._backend.run_query(query)
        for row in result:
            d = dict(zip(self._column_names, row))
            _key = d[self._key_col]
            d.pop(self._key_col)

            yield (_key, d)

    def pop(self, key):
        d = self.__getitem__(key)
        query = "DELETE FROM %s WHERE %s='%s'" % (self._table_name, self._key_col, key)
        self._backend.fetch_one(query)
        return d

    def insert(self, d):
        raise NotImplementedError

    def empty_table(self):
        raise NotImplementedError

    def create_table(self):
        cc = []
        for col_name, col_type in self._columns:
            if col_type == int:
                col_type = 'INTEGER'
            elif col_type == str:
                col_type = 'TEXT'

            if col_name == self._key_col:
                extras = 'PRIMARY KEY ASC'
            else:
                extras = ''

            cc.append("%s %s %s" % (col_name, col_type, extras))
        cc = ','.join(cc)
        query = "CREATE TABLE %s (%s)" % (self._table_name, cc)
        self._backend.run_query(query)

class AutoIncrementTable(object):
    def __init__(self, backend, table_name):
        self._backend = backend
        #self._engine = backend.connection
        self._table_name = table_name
        self._column_names = [ n for n, t in self._columns]
        assert not 'id' in self._column_names
        self._key_col_name = 'id'
        self._key_col = 'id'

    def _row_to_value(self, key, row):
        assert isinstance(row, tuple)
        row_key = self._key_col
        row_value = SqlRowProxy(self, key, row)
        return row_key, row_value

    def __len__(self):
        query = "SELECT count() FROM %s" % self._table_name
        ret = self._backend.run_query(query)
        return ret.fetchone()[0]

    def __contains__(self, key):
        query = "SELECT * FROM %s WHERE %s='%s'" % (self._table_name, self._key_col, key)
        row = self._backend.fetch_one(query)
        return row is not None

    def __setitem__(self, key, value):
        assert isinstance(value, dict)
        v, cn = set(value), set(self._column_names)
        assert not v - cn, repr(v - cn)
        assert not cn - v, repr(cn - v)
        assert set(value) == set(self._column_names), "%s %s" % (repr(set(value)), repr(set(self._column_names)))
        col_values = [key] + [value[k] for k in self._column_names]
        col_names = self._key_col_name+ ', ' + ', '.join(self._column_names)
        question_marks = ', '.join('?' for x in col_values)
        query = "REPLACE INTO %s (%s) VALUES (%s)" % (self._table_name, col_names, question_marks)
        ret = self._backend.run_query_using_conversion(query, col_values)
        #self._engine.commit()

    def append(self, value):
        assert isinstance(value, dict)
        v, cn = set(value), set(self._column_names)
        assert not v - cn, repr(v - cn)
        assert not cn - v, repr(cn - v)
        assert set(value) == set(self._column_names), "%s %s" % (repr(set(value)), repr(set(self._column_names)))
        col_values = [value[k] for k in self._column_names]
        col_names = ', '.join(self._column_names)
        question_marks = ', '.join('?' for x in col_values)
        query = "INSERT INTO %s (%s) VALUES (%s)" % (self._table_name, col_names, question_marks)
        ret = self._backend.run_query_using_conversion(query, col_values)
        #self._engine.commit()

    def __getitem__(self, key):
        query = "SELECT * FROM %s WHERE %s='%s'" % (self._table_name, self._key_col, key)
        row = self._backend.fetch_one(query)
        if row is None:
            raise KeyError(key)
        return self._row_to_value(key, row)[1]

    def __iter__(self):
        query = "SELECT %s FROM %s" % (self._key_col, self._table_name)
        result = self._backend.run_query(query)
        for row in result:
            yield row[0]

    def iteritems(self):
        query = "SELECT * FROM %s ORDER BY id DESC" % self._table_name
        result = self._backend.run_query(query)
        for row in result:
            _id = row[0]
            d = dict(zip(self._column_names, row[1:]))
            yield (_id, d)

    def delete(self, key):
        d = self.__getitem__(key)
        query = "DELETE FROM %s WHERE %s='%s'" % (self._table_name, self._key_col, key)
        self._backend.fetch_one(query)
        #self._engine.commit()
        return d

    def insert(self, d):
        raise NotImplementedError

    def empty_table(self):
        raise NotImplementedError

    def create_table(self):
        cc = ['id INTEGER PRIMARY KEY AUTOINCREMENT']
        for col_name, col_type in self._columns:
            if col_type == int:
                col_type = 'INTEGER'
            elif col_type == str:
                col_type = 'TEXT'

            cc.append("%s %s" % (col_name, col_type))
        cc = ','.join(cc)
        query = "CREATE TABLE %s (%s)" % (self._table_name, cc)
        self._backend.run_query(query)

class SingleValueTable(Table):
    def __init__(self, *args):
        super(SingleValueTable, self).__init__(*args)
        self._value_col = self._column_names[1]

    def __setitem__(self, key, value):
        assert not isinstance(value, dict)
        query = "INSERT OR REPLACE INTO %s (%s, %s) VALUES (?, ?)" % (self._table_name, self._key_col, self._value_col)
        col_values = (key, value)
        ret = self._backend.run_query_using_conversion(query, col_values)
        #self._engine.commit()

    def __getitem__(self, key):
         query = "SELECT %s FROM %s WHERE %s='%s'" % (self._value_col, self._table_name, self._key_col, key)
         row = self._backend.fetch_one(query)
         if row is None:
             raise KeyError(key)
         return row[0]

class UsersTable(Table):
    def __init__(self, *args, **kwargs):
        self._columns = (
                ('username', str),
                ('role', str),
                ('hash', str),
                ('email_addr', str),
                ('desc', str),
                ('create_date', str),
                ('last_login', str),
                ('git', str)
            )
        super(UsersTable, self).__init__(*args, **kwargs)

class RolesTable(SingleValueTable):
    def __init__(self, *args, **kwargs):
        self._columns = (
                ('role', str),
                ('level', int)
            )
        super(RolesTable, self).__init__(*args, **kwargs)

class JobsTable(AutoIncrementTable):
    def __init__(self, *args, **kwargs):
        self._columns = (
                ('name', str),
                ('create_date', str),
                ('create_user', str),
                ('branch', str),
                ('conf', str),
                ('arch', str),
                ('start_time', str),
                ('end_time', str),
                ('iso', str),
                ('status', int),
                ('desc', str),
                ('log', str),
                ('update_pxe', int),
                ('mail_successful', int),
                ('mail_failed', int)
            )
        super(JobsTable, self).__init__(*args, **kwargs)

class PendingRegistrationsTable(Table):
    def __init__(self, *args, **kwargs):
        self._columns = (
                ('code', str),
                ('username', str),
                ('role', str),
                ('hash', str),
                ('email_addr', str),
                ('desc', str),
                ('creation_date', str)
            )
        super(PendingRegistrationsTable, self).__init__(*args, **kwargs)

class SQLiteBackend(object):
    def __init__(self, db_name, users_tname='users', roles_tname='roles', pending_reg_tname='register', jobs_tname='jobs', initialize=False):
        self._db_name = db_name

        self.users = UsersTable(self, users_tname)
        self.jobs = JobsTable(self, jobs_tname)
        self.roles = RolesTable(self, roles_tname)
        self.pending_registrations = PendingRegistrationsTable(self, pending_reg_tname)
        BuilderLog()


        self.lock = threading.Lock()

        if initialize:
            self.users.create_table()
            self.jobs.create_table()
            self.roles.create_table()
            BuilderLog.debug("Table created")

    @property
    def connection(self):
        try:
            return self._connection
        except AttributeError:
            self._connection = sqlite3.connect(self._db_name, check_same_thread=False)
            return self._connection

    def get_conn(self):
        _connection = sqlite3.connect(self._db_name)
        return _connection

    def close_conn(self, conn=None):
        conn.close()
        
    def commit_conn(self, conn=None):
        conn.commit()

    def conn_trans(func):
        def _connection(self, *args, **kwargs):
            self.lock.acquire()
            conn = self.get_conn()
            kwargs['conn'] = conn
            rs = func(self, *args, **kwargs)
            self.commit_conn(conn)
            self.close_conn(conn)
            self.lock.release()
            return rs
        return _connection

    @conn_trans
    def run_query(self, query, conn=None):
        return conn.execute(query).fetchall()

    @conn_trans
    def run_query_using_conversion(self, query, args, conn=None):
        return conn.execute(query, args).fetchall()

    @conn_trans
    def fetch_one(self, query, conn=None):
        return conn.execute(query).fetchone()

    def save_users(self):
        pass

    def save_roles(self):
        pass

    def save_pending_registrations(self):
        pass
