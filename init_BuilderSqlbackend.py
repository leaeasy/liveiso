import os
import datetime
from BuilderSqlbackend import SQLiteBackend
from BuilderConfig import BuilderConfig

db = BuilderConfig().get('build','database')


if os.path.exists(db):
    os.unlink(db)

b = SQLiteBackend(db, initialize=True)

b.connection.executescript("""
        INSERT INTO users (username, email_addr, desc, role, hash, create_date) VALUES
             (
             'admin',
             'admin@localhost.local',
             'admin test user',
             'admin',
             'cLzRnzbEwehP6ZzTREh3A4MXJyNo+TV8Hs4//EEbPbiDoo+dmNg22f2RJC282aSwgyWv/O6s3h42qrA6iHx8yfw=',
             '2012-10-28 20:50:26.286723'
             );
         INSERT INTO roles (role, level) VALUES ('special', 200);
         INSERT INTO roles (role, level) VALUES ('admin', 100);
         INSERT INTO roles (role, level) VALUES ('editor', 60);
         INSERT INTO roles (role, level) VALUES ('user', 50);
        """)
