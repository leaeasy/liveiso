#!/usr/bin/env python3

import os
import subprocess
import tempfile
import glob

class Job():
    """git checkout --force --no-track -B isobuilder $ref"""
    def __init__(self, git, ref="master", dist="debian", log=None):
        self.env = os.environ.copy()
        self.env["LC_ALL"] = "C"
        self.dist = dist
        self.ref = ref
        self.git = git
        self.log = log

    def init(self):
        os.makedirs('/var/cache/isobuilder', exist_ok=True)
        self.build_space = tempfile.mkdtemp(prefix='/var/cache/isobuilder/%s-' % self.dist)
        status = subprocess.Popen(['git', 'clone', self.git.repo, 'config'], cwd=self.build_space, bufsize=0, stdout=self.log, stderr=subprocess.STDOUT, env=self.env).wait()
        if status != 0:
            raise OSError("Cannot clone git to work space!")
        subprocess.Popen(['git', 'checkout', self.ref], cwd=os.path.join(self.build_space,'config'), bufsize=0, stdout=self.log, stderr=subprocess.STDOUT, env=self.env).wait()
        status = subprocess.Popen(['git', 'checkout', '--force', '--no-track', '-B', 'isobuilder', self.ref], cwd=os.path.join(self.build_space,'config'), bufsize=0, stdout=self.log, stderr=subprocess.STDOUT, env=self.env).wait()
        if status != 0:
            raise OSError("Cannot checkout  git to work space!")

    def tuning_environment(self, config=None):
        pass

    def run(self, build_command=None, arch=None, name=None):
        self.env['_COLOR'] = "false"
        if self.dist == "debian":
            if build_command is None:
                #TODO:fix build_command path
                build_command="/home/isobuilder/isobuilder/backends/debian.py"
            if arch:
                build_command += " -a %s" % arch
            if name:
                build_command += " -n %s" % name
            statue = subprocess.Popen(build_command, cwd=self.build_space, shell=True, bufsize=0, stdout=self.log, stderr=subprocess.STDOUT, env=self.env).wait()
            return statue
        else:
            raise NotImplementedError

    def regress(self, dest_dir=None, rsync=True):
        if dest_dir is None:
            subprocess.Popen("rm -rf %s" % self.build_space, shell=True).wait()
            return 100,None
        os.makedirs(dest_dir, exist_ok=True)
        # Should just have only one iso file.
        _file = glob.glob(os.path.join(self.build_space, '*.iso'))[0]
        if rsync:
            status = subprocess.Popen("rsync -q %s %s" % (_file, dest_dir), shell=True).wait()
        else:
            _f = os.path.basename(_file)
            status = subprocess.Popen("install -Dm644 %s %s" % (_file, os.path.join(dest_dir,_f)), shell=True).wait()
        subprocess.Popen("rm -rf %s" % self.build_space, shell=True).wait()
        return status, os.path.basename(_file)

if __name__ == "__main__":
    from BuilderGit import BuilderGit
    git = BuilderGit(user='admin', git_url='/home/isobuilder/iso/i3/config')
    job = Job(git=git)
    job.init()
    job.tuning_environment()
    job.run()
    job.regress(dest_dir="/tmp/iso")
