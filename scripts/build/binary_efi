#!/bin/sh

set -e

. "${PROGRAM}/scripts/build.sh"

Echo_message "Begin preparing EFI support..."
Require_stagefile .build/config .build/bootstrap

Check_stagefile .build/binary.efi
Check_lockfile .lock
Create_lockfile .lock

case "${LB_ARCHITECTURE}" in
    amd64)
        _EFI_TYPE=efi64
        ;;
    *)
        echo "ERROR: can't provide EFI boot support to architecture ${LB_ARCHITECTURE}" >&2
        exit 0
        ;;
esac

case "${LIVE_IMAGE_TYPE}" in
    iso*)
        _BOOTLOADER="isolinux"
        _CONFDIR="binary/isolinux"
        ;;
    netboot)
        _BOOTLOADER="pxelinux"
        _CONFDIR="tftpboot"
        ;;
    hdd*|*)
        echo "TODO: can't provide EFI boot support to ${LIVE_IMAGE_TYPE}" >&2
        exit 0
        ;;
esac

_SYSLINUX_EFI_DIR="/usr/lib/SYSLINUX.EFI/${_EFI_TYPE}"
_SYSLINUX_MODULE_DIR="/usr/lib/syslinux/modules/${_EFI_TYPE}"


for _FILE in /usr/bin/syslinux /usr/lib/syslinux /usr/bin/mcopy /sbin/mkfs.msdos;do
    if [ ! -e ${_FILE} ];then
        Echo_error "${_FILE} - no such file."
        exit 1
    fi
done

if [ ! -e ${_SYSLINUX_EFI_DIR}/syslinux.efi ];then
    Echo_warning "${_SYSLINUX_EFI_DIR}/syslinux.efi missing, no EFI support included."
fi

case "${LB_INITRAMFS}" in
    casper)
        INITFS="casper"
        ;;
    live-boot)
        INITFS="live"
        ;;
    *)
        INITFS="boot"
        ;;
esac

rm -rf chroot/root/efi-temp chroot/root/efi.img binary/boot/efi.img binary/EFI
mkdir -p chroot/root/efi-temp/${INITFS}
mkdir -p chroot/root/efi-temp/EFI/BOOT
for _F in ${_CONFDIR}/*.cfg ${_CONFDIR}/*.png ${_SYSLINUX_MODULE_DIR}/*.*32 ${_SYSLINUX_MODULE_DIR}/*.*64 ${_CONFDIR}/bootlogo;do
    if [ -e ${_F} ];then
        cp ${_F} chroot/root/efi-temp/EFI/BOOT
    fi
done

cp ${_SYSLINUX_EFI_DIR}/syslinux.efi chroot/root/efi-temp/EFI/BOOT/syslinux.efi

case "$_EFI_TYPE" in
    efi64)
        cp ${_SYSLINUX_EFI_DIR}/syslinux.efi chroot/root/efi-temp/EFI/BOOT/BOOTX64.efi
	;;
    *)
	echo "Some thing smell wrong: $_EFI_TYPE" >&2
	;;
esac

cp binary/"${INITFS}"/vmlinuz* chroot/root/efi-temp/"${INITFS}"
cp binary/"${INITFS}"/initrd.img* chroot/root/efi-temp/"${INITFS}"

if [ -e chroot/root/efi-temp/EFI/BOOT/menu.cfg ];then
    sed -i -e "s/^menu title/menu title EFI/" chroot/root/efi-temp/EFI/BOOT/menu.cfg
else
    Echo "No menu.cfg file that can be edited to indicate that we boot on EFI!"
fi

if [ ! -e chroot/root/efi-temp/EFI/BOOT/syslinux.cfg ];then
    for _F in isolinux.cfg pxelinux.cfg/default extlinux.conf;do
        if [ -f chroot/root/efi-temp/EFI/BOOT/$_F ];then
            cp chroot/root/efi-temp/EFI/BOOT/$_F chroot/root/efi-temp/EFI/BOOT/syslinux.cfg
            break
        fi
    done
fi

case "${LIVE_BOOT_SCREEN}" in
	gfxboot)
		#TODO: check if bootlogo exits
		if [ ! -e chroot/root/efi-temp/EFI/BOOT/gfxboot.c32 ];then
			cp ${_SYSLINUX_MODULE_DIR}/gfxboot.c32 chroot/root/efi-temp/EFI/BOOT/
		fi
		sed -i "s/@@BOOTSCREEN_ARGS@@/ui\ gfxboot\ bootlogo/g" chroot/root/efi-temp/EFI/BOOT/syslinux.cfg
		;;
	*)
		if [ ! -e chroot/root/efi-temp/EFI/BOOT/vesamenu.c32 ];then
			cp ${_SYSLINUX_MODULE_DIR}/vesamenu.c32 chroot/root/efi-temp/EFI/BOOT/
		fi
		sed -i "s/@@BOOTSCREEN_ARGS@@/default\ vesamenu.c32/g" chroot/root/efi-temp/EFI/BOOT/syslinux.cfg
		;;
esac

_TOTALSIZE=$(du -sk chroot/root/efi-temp/ | awk '{print $1}')
# Add 5% safety margin
_TOTALSIZE=$(( $_TOTALSIZE * 21 / 20 ))
# Required size rounded to upper 32kb
_BLOCKS=$(( ($_TOTALSIZE + 31) / 32 * 32 ))
Echo "EFI boot image needs $_TOTALSIZE Kb, thus allocating $_BLOCKS blocks."

mkfs.msdos -C chroot/root/efi.img ${_BLOCKS} >/dev/null
mcopy -s -v -i chroot/root/efi.img chroot/root/efi-temp/* :: >/dev/null 2>&1

mkdir -p binary/boot
mv chroot/root/efi.img binary/boot/
mv chroot/root/efi-temp/EFI binary
rm -rf chroot/root/efi-temp

Create_stagefile .build/binary.efi
