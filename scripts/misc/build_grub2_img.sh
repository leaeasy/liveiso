#!/bin/sh

input_dir=/usr/lib/grub/i386-pc

# build core.img
core_image=$(mktemp)
grub-mkimage -d ${input_dir} -o ${core_image} -O i386-pc biosdisk iso9660

# build grub_eltorito image
cat ${input_dir}/cdboot.image ${core_image} > binary/boot/grub/grub_eltorito

rm -f ${core_image}

for file in ${input_dir}/*.mod ${input_dir}/efiemu??.o ${input_dir}/*.lst;do
    if test -f "$file";then
        cp -f "$file" binary/boot/grub
    fi
done
