#!/bin/sh

set -e 

#source functions
for _FILE in "${PROGRAM}"/functions/*.sh;do
    if [ -e "${_FILE}" ];then
        . "${_FILE}"
    fi
done
