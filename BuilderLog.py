#! /usr/bin/env python

import logging
from BuilderConfig import BuilderConfig

class BuilderLog(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init()
        return cls._instance

    def init(self):
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s',
                filename=BuilderConfig().get('log','file'), datefmt='%Y-%m-%d %H:%M:%S', filemode='a')
    
    @classmethod
    def debug(self, str):
        logging.debug(str)

    @classmethod
    def info(self, str):
        logging.info(str)

    @classmethod
    def warn(self, str):
        logging.warning(str)

    @classmethod
    def error(self, str):
        logging.error(str)
