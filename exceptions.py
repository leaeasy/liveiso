#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#

class InitException(Exception):
    def __init__(self, info):
        self.info = info
    def __str__(self):
        return("E: Init exception - %s" % self.info)

class DebootstrapException(Exception):
    def __init__(self, info):
        self.info = info

    def __str__(self):
        return("E: Debootstrap exception - %s" % self.info)

class ExecutionException(Exception):
    def __init__(self, command, status, stderr=None):
        self.stderr = stderr
        self.status = status
        self.command = command

    def __str__(self):
        if self.stderr is not None:
            return ("'%s' returned exit status %s: %s" % (self.command, self.status, self.stderr))
        else:
            return ("'%s' returned exit status %s" % (self.command, self.status))
