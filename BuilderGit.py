#!/usr/bin/env python3
import os
import subprocess
from BuilderConfig import BuilderConfig

class BuilderGit():
    def __init__(self, user, git_url):
        self.user= user
        self.cfg = BuilderConfig()
        self._base_config = self.cfg.get('build','base_config')
        self.base_config = os.path.join(self._base_config, self.user)
        self.git_url = git_url
        self.repo = os.path.join(self.base_config, 'config.git')
        self.init()

    def execute(self, subprocess_args, cwd):
        execute_cmd = ['git']+list(subprocess_args)
        proc = subprocess.Popen(execute_cmd, cwd=cwd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        try:
            stdout, stderr = proc.communicate()
            status = proc.returncode
            if stdout.endswith(b'\n'):
                stdout = stdout[:-1]
            if stderr.endswith(b'\n'):
                stderr = stdout[:-1]
        finally:
            proc.stdout.close()
            proc.stderr.close()

        if status !=0:
            raise OSError("Git command:%s error" % " ".join(execute_cmd))
        
        return stdout.decode()

    def init(self):
        if not os.path.exists(self.base_config):
            os.makedirs(self.base_config)

        if not os.path.exists(self.repo):
            self.execute(['clone', '--bare', self.git_url, 'config.git'], cwd=self.base_config)

        else:
            self.update()

    @property
    def url(self):
        try:
            for i in self.execute(['remote','-v'], cwd=self.repo).split('\n'):
                if "origin" in i and "(fetch)" in i:
                    _url = i.split()[1]
        except:
            _url = None
        return _url
    
    def update(self):
        if self.url == self.git_url:
            self.execute(['fetch', 'origin', '+refs/heads/*:refs/heads/*'], cwd=self.repo)
        else:
            os.system("rm -rf %s" % self.repo)
            self.execute(['clone', '--bare', self.git_url, 'config.git'], cwd=self.base_config)


    @property
    def tags(self):
        self.update()
        _tags = []
        for i in self.execute(['tag'], cwd=self.repo).split('\n'):
            _tags.append(i)
        return _tags

    @property
    def branchs(self):
        self.update()
        _branchs = []
        for i in self.execute(['branch','--no-color'], cwd=self.repo).split('\n'):
            if len(i) > 2:
                _branchs.append(i[2:])
        return _branchs

class BuilderLocal():
    def __init__(self, user, url="config"):
        self.user= user
        self.cfg = BuilderConfig()
        self._base_config = self.cfg.get('build','base_config')
        self.base_config = os.path.join(self._base_config, self.user)
        self.url = url
        self.repo = os.path.join(self.base_config, self.url)

if __name__ == '__main__':
    b = BuilderGit('admin','/home/deepin/iso.config/i3/config')
    print(b.branchs)
