install:
	@if [ -d /usr/share/isobuilder ];then rm -rf /usr/share/isobuilder; fi
	@mkdir -p /usr/share/isobuilder
	cp -ar * /usr/share/isobuilder/
	install -Dm644 builderrc /etc/isobuilder/builderrc
initdb:
	python3 init_BuilderSqlbackend.py

clean:
	rm -rf /usr/share/isobuilder
