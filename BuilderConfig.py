#! /usr/bin/env python3

import configparser
import os

class BuilderConfig(configparser.ConfigParser):
    config_file = "/etc/isobuilder/builderrc"

    def __init__(self, *args, **kwargs):
        configparser.ConfigParser.__init__(self)
        self.reload()

    def init(self):
        configparser.ConfigParser.__init__(self)
        self.add_section('build')
        self.add_section('log')
        self.add_section('mail')

        self.set('build', 'check_every', '300')
        self.set('build', 'max_jobs', '2')
        self.set('build', 'database', 'builder.db')


        self.set('log', 'file', './log/isobuilder.log')
        self.set('log', 'logs_dir', './log/build_logs')

        self.set('mail', 'mail_from', 'isobuilder@linuxdeepin.com')
        self.set('mail', 'subject_prefix', '[ISOBUILDER]')
        self.set('mail', 'smtp_host', 'localhost')
        self.set('mail', 'smtp_port', '25')
        self.save()
        self.reload()

    def reload(self):
        return self.read(self.config_file)

    def dump(self):
        conf = ""
        for section in self.sections():
            conf += '[' + section + ']\n'
            for item, value in self.items(section):
                conf += "%s=%s\n" % (item, value)
            conf += '\n'
        return conf

    def save(self):
        try:
            self.write(open(self.config_file, 'w'))
        except Exception as error:
            print(error)
            return False
        return True
        
if __name__ == '__main__':
    s = BuilderConfig()
    s.init()
    s.dump()
