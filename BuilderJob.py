#!/usr/bin/env python3

from BuilderConfig import BuilderConfig
from BuilderSqlbackend import SQLiteBackend
from BuilderGit import BuilderGit
import enum
import threading
from Job import Job
from datetime import datetime
import sys
import os
import subprocess

class BuildStatus(enum.Enum):
    WAIT = 0
    WAIT_LOCK = 100
    BUILDING = 200
    BUILD_FAILED = 500
    UPLOAD_FAILED = 700
    BUILD_OK = 1000

class BuilderJob(threading.Thread, SQLiteBackend):
    notify = None
    def __init__(self, job):
        self.cfg = BuilderConfig()
        self.db = self.cfg.get('build','database')
        self.job = job
        self.notify = None

        threading.Thread.__init__(self)
        self.do_quit = threading.Event()
        self.status_lock = threading.Lock()

    def upload(self, logfile=None):
        update_pxe_command = self.cfg.get('build','update_pxe_cmd')
        isofile = self.job[1]['iso']
        _user = self.job[1]['create_user']
        title = "%d-%s" % (self.job[0], self.job[1]['name'])
        cmd = "%s %s %s %s" % (update_pxe_command, isofile, _user, title)
        subprocess.Popen(cmd.split(), bufsize=0, stdout=logfile, stdin=None, stderr=subprocess.STDOUT).wait()

    
    @property
    def logfile(self):
        logs_dir = self.cfg.get('log','logs_dir')
        if not os.path.exists(logs_dir):
            os.makedirs(logs_dir)
        return "%s/%s" % (logs_dir, self.job[1]['log'])

    def preexec_child(self):
        os.setsid()

    def run(self):
        try:
            with open(self.logfile, 'w') as build_log:
                build_log.write("Start build ID: %d\n" % self.job[0])
        except IOError as e:
            print(e)
            return

        build_log = open(self.logfile, 'a')
        with self.status_lock:
            self.job[1]['status'] = BuildStatus.BUILDING.value
            self.job[1]['start_time'] = str(datetime.utcnow())
            SQLiteBackend(self.db).jobs[self.job[0]] = self.job[1]

        try:
            _user = self.job[1]['create_user']
            if self.job[1]['arch'] != "":
                _arch = self.job[1]['arch']
            else:
                _arch = None

            _git_url = SQLiteBackend(self.db).users[_user]['git'] 
            _user_builder_git = BuilderGit(user=_user, git_url=_git_url)
            _ref = self.job[1]['branch']
            if _ref:
                job = Job(git=_user_builder_git, ref=_ref, log=build_log)
            else:
                job = Job(git=_user_builder_git, log=build_log)

            job.init()
            job.tuning_environment()
            _status = job.run(arch=_arch, name="%d-%s"% (self.job[0],self.job[1]['name']))
            build_log.write("End build status: %d\n" % _status)
            if _status == 0:
                _dir = self.cfg.get('build','output')
                timestamp=datetime.now().strftime("%Y%m%d")
                dest_dir = os.path.join(_dir.replace("${user}", _user),timestamp) 
                _dest_dir = os.path.join(_user, timestamp)
                _status, iso = job.regress(dest_dir=dest_dir)
            else:
                job.regress()
        except Exception as e:
            print(e)
            job.regress()
            _status = 1

        with self.status_lock:
            if _status != 0:
                self.job[1]['status'] = BuildStatus.BUILD_FAILED.value
                self.job[1]['end_time'] = str(datetime.utcnow())
                SQLiteBackend(self.db).jobs[self.job[0]] = self.job[1]
            else:
                self.job[1]['end_time'] = str(datetime.utcnow())
                self.job[1]['status'] = BuildStatus.BUILD_OK.value
                self.job[1]['iso'] = str(os.path.join(_dest_dir, iso))
                SQLiteBackend(self.db).jobs[self.job[0]] = self.job[1]

        build_log.write("\n***************************************************\n")
        if _status == 0 and self.job[1]['update_pxe'] == 1:
            build_log.write("Begin update pxe...\n")
            update_pxe_command = self.cfg.get('build','update_pxe_cmd')
            if update_pxe_command:
                isofile = os.path.join(dest_dir, iso)
                title = "%d-%s" % (self.job[0], self.job[1]['name'])
                cmd = "%s %s %s %s" % (update_pxe_command, isofile, _user, title)
                try:
                    build_log.write("Updating pxe: %s \n" % cmd)
                    proc = subprocess.Popen(cmd.split(), bufsize=0, stdout=build_log, stdin=None, stderr=subprocess.STDOUT).wait()
                except Exception as error:
                    build_log.write("\nUnable to execute command \"%s \": %s" % (cmd, error))
                    self.job[1]['status'] = BuildStatus.UPLOAD_FAILED.value
                    SQLiteBackend(self.db).jobs[self.job[0]] = self.job[1]
            else:
                build_log.write("SKIPED!!!No upload script found!\n")

        build_log.close()
        if self.notify:
            self.notify.set()

if __name__ == '__main__':
    sql = SQLiteBackend(BuilderConfig().get('build','database'), initialize=False)
    job = None
    for i in sql.jobs.iteritems():
        b = BuilderJob(i)
        print(b.upload())
        break

    #for j in sql.jobs.iteritems():
    #    if j[1]['status'] == BuildStatus.WAIT.value:
    #        print(j)
    #        print('----')
    #        job = j
    #        break
    #if job is not None:
    #    b = BuilderJob(job)
    #    b.run()
