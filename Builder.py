from BuilderLog import BuilderLog
from BuilderConfig import BuilderConfig
from BuilderSqlbackend import SQLiteBackend
import threading
import signal
import os
import enum
import sys
from BuilderJob import BuilderJob, BuildStatus


class Builder(object):
    jobs = []
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._instance.init()
        return cls._instance

    def init(self):
        self.cfg = BuilderConfig()

        BuilderLog()
        self.db = self.cfg.get('build', 'database')
        self.do_quit = threading.Event()
        self.jobs_locker = threading.Lock()
        self.job_finshed = threading.Event()
        self.max_jobs = self.cfg.getint('build','max_jobs')

    def daemonize(self):
        signal.signal(signal.SIGTERM, self.handle_sigterm)
        signal.signal(signal.SIGINT, self.handle_sigterm)
        #try:
        #    sys.stdout = sys.stderr = open(self.cfg.get('log', 'file'), 'a')
        #except Exception as error:
        #    print("E: unable open logfile: %s" % error)

    def loop(self):
        counter = self.cfg.getint('build', 'check_every')
        while not self.do_quit.isSet():
            if counter == self.cfg.getint('build', 'check_every') or self.job_finshed.isSet():
                self.get_new_jobs()
                self.start_jobs()
                self.clean_jobs()
                counter = 0
                self.job_finshed.clear()
            self.do_quit.wait(1)
            counter += 1

    def clean_jobs(self):
        with self.jobs_locker:
            for job in self.jobs:
                _status = job[1]['status']
                if _status == BuildStatus.BUILD_OK.value or _status == BuildStatus.BUILD_FAILED.value:
                    self.jobs.remove(job)
        return True

    def daemon(self):
        BuilderLog.info("Staring IsoBuilder")
        self.daemonize()

        BuilderLog.info("Running main loop")
        self.loop()

        BuilderLog.info("Cleaning finished and canceled jobs")
        self.clean_jobs()
        BuilderLog.info("Stopping all jobs")
        self.stop_all_jobs()
        BuilderLog.info("Releasing wait-locked jobs")
        self.release_jobs()
        BuilderLog.info("Exiting IsoBuilder")

    def stop_all_jobs(self):
        return True

    def release_jobs(self):
        return True

    def list_jobs(self):
        for job in SQLiteBackend(self.db).jobs.iteritems():
            yield job

    def fetch_job(self):
        count=0
        jobs=[]
        for job in SQLiteBackend(self.db).jobs.iteritems():
            if count >= self.max_jobs:
                break
            if job[1]['status'] == BuildStatus.WAIT.value:
                _flag = True
                for _job in self.jobs:
                    if int(job[0]) == int(_job[0]):
                        _flag = False
                        break
                if _flag:
                    print("change %d to WAIT_LOCK" % job[0])
                    job[1]['status'] = BuildStatus.WAIT_LOCK.value
                    SQLiteBackend(self.db).jobs[job[0]] = job[1]
                    jobs.append(job)
                    count += 1
        return jobs[:self.max_jobs]

    def get_job(self, jobid):
        for i, job in self.list_jobs():
            if i == jobid:
                return job
        return None


    def get_new_jobs(self):
        count_current = len(self.jobs)
        with self.jobs_locker:
            if count_current >= self.max_jobs:
                return 0
            jobs = self.fetch_job()
            count_new = 0
            for job in jobs:
                self.jobs.append(job)
                count_new += 1
                count_current += 1

                if count_current >= self.max_jobs:
                    break
            return count_new

    def count_running_jobs(self):
        count = 0
        with self.jobs_locker:
            for job in self.jobs:
                if job[1]['status'] == BuildStatus.BUILDING.value:
                    count += 1
        return count

    def start_jobs(self):
        running_jobs = self.count_running_jobs()
        jobs_started = 0
        with self.jobs_locker:
            for job in self.jobs:
                if running_jobs >= self.max_jobs:
                    break
                if job[1]['status'] == BuildStatus.WAIT_LOCK.value:
                    BuilderLog.info("Staring new thread for job %s" % job[0])
                    _job = BuilderJob(job)
                    _job.notify = self.job_finshed
                    _job.setDaemon(True)
                    _job.start()
                    jobs_started += 1
                    running_jobs += 1
        return jobs_started

    def handle_sigterm(self, signum, stack):
        BuilderLog.info("Receiving transmissiong... it's a signal %s captain! EVERYNON OUT!" % signum)
        self.do_quit.set()

if __name__ == "__main__":
    Builder().daemon()
